from fastapi import FastAPI, Body

app = FastAPI()

class NodoLista:
    def __init__(self, valor):
        self.valor = valor
        self.siguiente = None

class AlgoritmoLiebreTortuga:
    @staticmethod
    def tiene_ciclo(cabeza):
        if cabeza is None or cabeza.siguiente is None:
            return False

        tortuga = cabeza
        liebre = cabeza.siguiente

        while tortuga != liebre:
            if liebre is None or liebre.siguiente is None:
                return False
            tortuga = tortuga.siguiente
            liebre = liebre.siguiente.siguiente

        return True

class BusquedaRepetidos:
    @staticmethod
    def encontrar_rep(numeros):
        tortuga = numeros[numeros[0]]
        liebre = numeros[numeros[numeros[0]]]

        while tortuga != liebre:
            tortuga = numeros[tortuga]
            liebre = numeros[numeros[liebre]]

        ptr1 = numeros[0]
        ptr2 = tortuga

        while ptr1 != ptr2:
            ptr1 = numeros[ptr1]
            ptr2 = numeros[ptr2]

        return ptr1

@app.post("/")
def main(numeros: list = Body(..., embed=True)):

    numeros = [int(num) for num in numeros]
    # Crear una lista con ciclo
    nodo1 = NodoLista(1)
    nodo2 = NodoLista(2)
    nodo3 = NodoLista(3)
    nodo4 = NodoLista(4)
    nodo1.siguiente = nodo2
    nodo2.siguiente = nodo3
    nodo3.siguiente = nodo4
    nodo4.siguiente = nodo2  # Esto crea un ciclo

    ciclo_presente = AlgoritmoLiebreTortuga.tiene_ciclo(nodo1)

    # Crear una lista sin ciclo
    nodo5 = NodoLista(5)
    nodo6 = NodoLista(6)
    nodo7 = NodoLista(7)
    nodo5.siguiente = nodo6
    nodo6.siguiente = nodo7

    ciclo_no_presente = AlgoritmoLiebreTortuga.tiene_ciclo(nodo5)

    numero_repetido = BusquedaRepetidos.encontrar_rep(numeros)

    return {
        'ciclo_presente': ciclo_presente,
        'ciclo_no_presente': ciclo_no_presente,
        'numero_repetido': numero_repetido
    }
